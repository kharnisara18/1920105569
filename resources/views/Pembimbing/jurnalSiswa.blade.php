@extends('layouts.pembimbing.dashboard')

@section('body')

    <div class="container mt-5">
        <div class="row ">
            <div class="col-lg-6">
                <table class="table table-striped table-responsive text-center">
                    <thead class="thead-inverse|thead-default">
                        <tr >
                            <th class="text-center">NIS</th>
                            <th class="text-center">Nama Lengkap</th>
                            <th class="text-center">aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                                <tr>
                                    <td scope="row">{{ $item->nis }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>
                                        <a href="/dashboard/Cek_kegiatan/{{ $item->id }}" class="btn btn-info">Detail</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                </table>
                
            </div>
            <div class="col-lg-6 mt-5">
                <img src="/img/logo.png" class="d-flex m-auto" alt="" style="width: 400px; height: 400px; opacity: 30%">
            </div>
        </div>
    </div>
    <button onclick="kembali()" class="btn btn-danger">Kembali</button>
    <script>function kembali()
    {
    window.history.back();
    }
    </script>
@endsection