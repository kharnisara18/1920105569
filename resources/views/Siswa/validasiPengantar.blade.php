@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card" >
            <div class="card-body">
                <h2 style="padding-top: 2px; ">Haii {{ Auth()->user()->name }} </h2><hr>    
                <h3>Saat ini Surat pengantar Sedang di validasi oleh pembimbing</h3>
                <h4>Anda bisa melanjutkan ke step selanjutnya setelah validasi selesai</h4>
                <a href="">Hubungi Pembimbing</a>
                <a href="/download/{{ Auth()->User()->pengantar_pkl }}" class="btn btn-warning btn-xm" >Download Surat pengantar</a>
            </div>        
        </div>
    </div>

@endsection